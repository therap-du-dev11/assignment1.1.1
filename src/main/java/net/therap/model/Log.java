package net.therap.model;

import java.util.Date;

/**
 * @author iftakhar.ahmed
 * @since 9/16/21
 */
public class Log {

    private Date dateAndTime;
    private String uri;
    private String httpMethod;
    private int responseTime;

    public Log(Date dateAndTime, String uri, String httpMethod, int responseTime) {
        this.dateAndTime = dateAndTime;
        this.uri = uri;
        this.httpMethod = httpMethod;
        this.responseTime = responseTime;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public int getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(int responseTime) {
        this.responseTime = responseTime;
    }
}