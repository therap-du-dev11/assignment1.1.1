package net.therap.model;

import java.util.Date;

/**
 * @author iftakhar.ahmed
 * @since 9/16/21
 */
public class LogOutput {

    private Date startDuration;
    private Date endDuration;
    private int getCount;
    private int postCount;
    private int uniqueUriCount;
    private int totalResponseTime;

    public LogOutput(Date startDuration, Date endDuration
            , int getCount, int postCount
            , int uniqueUriCount, int totalResponseTime) {
        this.startDuration = startDuration;
        this.endDuration = endDuration;
        this.getCount = getCount;
        this.postCount = postCount;
        this.uniqueUriCount = uniqueUriCount;
        this.totalResponseTime = totalResponseTime;
    }

    public Date getStartDuration() {
        return startDuration;
    }

    public void setStartDuration(Date startDuration) {
        this.startDuration = startDuration;
    }

    public Date getEndDuration() {
        return endDuration;
    }

    public void setEndDuration(Date endDuration) {
        this.endDuration = endDuration;
    }

    public int getGetCount() {
        return getCount;
    }

    public void setGetCount(int getCount) {
        this.getCount = getCount;
    }

    public int getPostCount() {
        return postCount;
    }

    public void setPostCount(int postCount) {
        this.postCount = postCount;
    }

    public int getUniqueUriCount() {
        return uniqueUriCount;
    }

    public void setUniqueUriCount(int uniqueUriCount) {
        this.uniqueUriCount = uniqueUriCount;
    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }

    public void setTotalResponseTime(int totalResponseTime) {
        this.totalResponseTime = totalResponseTime;
    }
}