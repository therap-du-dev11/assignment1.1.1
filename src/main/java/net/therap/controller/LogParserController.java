package net.therap.controller;

import net.therap.model.Log;
import net.therap.model.LogOutput;
import net.therap.service.LogParserService;
import net.therap.view.ResultViewer;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * @author iftakhar.ahmed
 * @since 9/16/21
 */
public class LogParserController {

    public static void init(String[] args) {
        LogParserService service = new LogParserService();
        ResultViewer viewer = new ResultViewer();
        try {
            List<Log> logs = service.getLogs(args[0]);
            List<LogOutput> summaries = service.process(logs);
            viewer.print(summaries, shouldSort(args));
        } catch (FileNotFoundException e) {
            System.out.println("Log file not found : Please specify " +
                    "the filename correctly and makesure that " +
                    "it is present on resource folder");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static boolean shouldSort(String[] args) throws Exception {
        if (args.length == 1) {
            return false;
        }
        String arg2 = "--sort";
        if (args.length == 2 && !args[1].equals(arg2)) {
            throw new Exception("Invalid Argument exception: 2nd argument should be --sort");
        }
        if (args.length == 2 && args[1].equals(arg2)) {
            return true;
        } else {
            throw new Exception("Too many arguments");
        }
    }
}