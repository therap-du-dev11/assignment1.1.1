package net.therap.service;

import net.therap.model.Log;
import net.therap.model.LogOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * @author iftakhar.ahmed
 * @since 9/17/21
 */
public class LogParserService {

    private final String POST="POST";
    private final String GET="GET";

    private Log processALog(String line) {
        SearchService searchService = new SearchService();
        String regexS = "\\d*-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d";
        Date date = null;
        String method = null;
        String uri = null;
        int time = 0;
        if (searchService.didPatternMatched(regexS, line)) {
            String dateS = searchService.getTheMatch(regexS, line);
            String[] tokens = dateS.split("[ :-]");
            Calendar calendar = Calendar.getInstance();
            calendar.set(Integer.parseInt(tokens[0])
                    , Integer.parseInt(tokens[1]) - 1
                    , Integer.parseInt(tokens[2])
                    , Integer.parseInt(tokens[3])
                    , Integer.parseInt(tokens[4])
                    , Integer.parseInt(tokens[5]));
            date = calendar.getTime();
        }
        if (searchService.didPatternMatched(", [Pp],", line)) {
            method = this.POST;
        }
        if (searchService.didPatternMatched(", [Gg],", line)) {
            method = this.GET;
        }
        if (searchService.didPatternMatched("URI=\\[.+\\]", line)) {
            String rawUri = searchService.getTheMatch("\\[.+\\]", searchService.getTheMatch("URI=\\[.+\\]", line));
            uri = rawUri.substring(1, rawUri.length() - 1).trim();
        }
        if (searchService.didPatternMatched("time ?= ?(\\d)+", line)) {
            String timeToken = searchService.getTheMatch("time ?= ?(\\d)+", line);
            String timeS = searchService.getTheMatch("\\d+", timeToken);
            time = Integer.parseInt(timeS);
        }

        Log log = new Log(date, uri, method, time);
        return log;
    }

    public List<Log> getLogs(String fileName) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(fileName));
        ArrayList<Log> logList = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            logList.add(processALog(line));
        }
        return logList;
    }

    public List<LogOutput> process(List<Log> logs) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        List<LogOutput> summaries = new ArrayList<>();
        for (int index = 0; index < 24; index++) {
            Date startDuration = calendar.getTime();
            calendar.add(Calendar.HOUR, 1);
            Date endDuration = calendar.getTime();
            SearchService searchService = new SearchService();
            List<Log> currentLogs = searchService.getLogsBetween(logs,startDuration, endDuration);
            int postCount = searchService.getMethodCount(currentLogs,this.POST);
            int getCount = searchService.getMethodCount(currentLogs,this.GET);
            int uniqueUriCount = searchService.getUniqueUriCount(currentLogs);
            int totalResponceTime = searchService.getTotalResponseTime(currentLogs);
            LogOutput summary = new LogOutput(startDuration, endDuration, getCount
                    , postCount, uniqueUriCount, totalResponceTime);
            summaries.add(summary);
        }
        return summaries;
    }
}