package net.therap.service;

import net.therap.model.Log;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author iftakhar.ahmed
 * @since 9/17/21
 */
public class SearchService {

    public int getMethodCount(List<Log> logs,String method) {
        int count = 0;
        for (Log log : logs) {
            if (method.equals(log.getHttpMethod())) {
                count++;
            }
        }
        return count;
    }

    public int getUniqueUriCount(List<Log> logs) {
        Set<String> set = new HashSet<>();
        for (Log log : logs) {
            set.add(log.getUri());
        }
        return set.size();
    }

    public int getTotalResponseTime(List<Log> logs) {
        int totalTime = 0;
        for (Log log : logs) {
            totalTime += log.getResponseTime();
        }
        return totalTime;
    }

    public boolean didPatternMatched(String pattern, String text) {
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(text);
        if (matcher.find()) {
            return true;
        }
        return false;
    }

    public String getTheMatch(String pattern, String text) {
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(text);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    @SuppressWarnings("deprecation")
    public List<Log> getLogsBetween(List<Log> logsP,Date startTime, Date endTime) {
        List<Log> logs = new ArrayList<>();
        for (Log log : logsP) {
            if (Objects.nonNull(log.getDateAndTime())) {
                long foundDate = (long) log.getDateAndTime().getHours() * (long) 3600
                        + (long) log.getDateAndTime().getMinutes() * 60L
                        + (long) log.getDateAndTime().getSeconds();
                long startTimeL = (long) startTime.getHours() * 3600L
                        + (long) startTime.getMinutes() * 60L
                        + (long) startTime.getSeconds();
                long endTimeL = (long) endTime.getHours() * 3600L
                        + (long) endTime.getMinutes() * 60L
                        + (long) endTime.getSeconds();
                if (foundDate >= startTimeL && foundDate < endTimeL) {
                    logs.add(log);
                }
            }
        }
        return logs;
    }
}