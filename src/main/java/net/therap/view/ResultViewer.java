package net.therap.view;

import net.therap.model.LogOutput;

import java.text.DateFormat;
import java.util.Comparator;
import java.util.List;

/**
 * @author iftakhar.ahmed
 * @since 9/17/21
 */
public class ResultViewer {

    public void print(List<LogOutput> summaries, boolean sort) {
        printHeadings();
        if (sort == false) {
            printValues(summaries);
        } else {
            summaries.sort(new MyComparator());
            printValues(summaries);
        }
    }

    private void printHeadings() {
        System.out.println(String.format("%35s%35s%35s%35s", "Time", "GET/POST COUNT", "Unique URI Count", "Total Response Time"));
    }

    private void printValues(List<LogOutput> values) {
        for (int i = 0; i < values.size(); i++) {
            System.out.print(String.format("%35s", DateFormat.getDateTimeInstance(3, 3)
                    .format(values.get(i).getStartDuration()) + "-" + DateFormat
                    .getDateTimeInstance(3, 3)
                    .format(values.get(i).getEndDuration())));
            System.out.print(String.format("%35s", Integer
                    .toString(values.get(i).getGetCount()) + "/"
                    + Integer.toString(values.get(i).getPostCount())));
            System.out.print(String.format("%35s", Integer.toString(values.get(i).getUniqueUriCount())));
            System.out.println(String.format("%35s", Integer.toString(values.get(i).getTotalResponseTime())));
        }
    }
}

class MyComparator implements Comparator<LogOutput> {
    @Override
    public int compare(LogOutput t, LogOutput t1) {
        if ((t.getPostCount() + t.getGetCount()) > (t1.getPostCount() + t1.getGetCount())) {
            return -1;
        } else if ((t.getPostCount() + t.getGetCount()) == (t1.getPostCount() + t1.getGetCount())) {
            return 0;
        } else {
            return +1;
        }
    }
}